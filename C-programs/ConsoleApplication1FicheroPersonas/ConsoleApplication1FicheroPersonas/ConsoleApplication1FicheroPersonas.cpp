// ConsoleApplication1FicheroPersonas.cpp: define el punto de entrada de la aplicación de consola.

/*V2RQ9-87NYX-TKRB3-MCWVB-7MHWP*/
/*Gestionar datos alumnos
-Nombre
-Edad
Maximo 10 Alumnos

1. Cargar datos
2. Escribir datos
3. Insertar tabla
4. Mostrar tabla pantalla
5. Borrar tabla(Huecos)
6. Salir
*/

#include "stdafx.h" //sirve para enlazar la consola con la aplicacion c++
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME "clase.txt"
#define FILENAME2 "clase2.txt"

/*--------------------------------------*/
/*Declaracion de la estructura de alumno*/
/*--------------------------------------*/
struct TAlumno
{
	char nombre[40];
	int edad;
};
/*--------------------------------------*/
/*Funciones			                    */
/*--------------------------------------*/
void cargarDatos(struct TAlumno tablaAlumnos[10], int *numero)
{
	FILE *pf;
	char nombreAlumno[40];
	int edadAlumno;
	int posicion;
	posicion = 0;
	(*numero) = 0;

	pf = fopen(FILENAME, "rt");
	if (pf != NULL)
	{
		while (!feof(pf))
		{
			fscanf (pf, "%s", nombreAlumno);
			fscanf (pf, "%i", &edadAlumno);
			strcpy (tablaAlumnos[posicion].nombre, nombreAlumno);
			tablaAlumnos[posicion].edad = edadAlumno;
			posicion++;
		}
		(*numero) = posicion;
		fclose(pf);
	}
}

void guardarDatos(struct TAlumno tablaAlumnos[10],int numero)
{
	FILE *pf;
	int posicion;

	pf = fopen(FILENAME2, "wt");
	if (pf != NULL)
	{
		for (posicion = 0; posicion < numero; posicion++)
		{
			fprintf(pf, "%s\n", tablaAlumnos[posicion].nombre);
			fprintf(pf, "%i\n", tablaAlumnos[posicion].edad);
		}
		fclose(pf);
	}
}

void imprimirPantalla(struct TAlumno tablaAlumnos[10], int numero)
{
	int posicion;

	for (posicion = 0; posicion < numero; posicion++)
	{
		printf( "%s\n", tablaAlumnos[posicion].nombre);
		printf( "%i\n", tablaAlumnos[posicion].edad);
	}
	printf("\n");

}

void insertarDatos(struct TAlumno tablaAlumnos[10], int numero)
{
	
}


void imprimirMenu()
{
	printf("MENU DE OPCIONES \n");
	printf("-------------------------------\n");
	printf("1. Cargar datos del fichero \n");
	printf("2. Guardar datos en el fichero \n");
	printf("3. Escribir tabla por patalla \n");
	printf("4. Insertar en la tabla \n");
	printf("5. Borrar en la tabla \n");
	printf("6. Salir \n");
	printf("Introduce la opcion: ");

}

int main()
{
	int opcion;
	struct TAlumno tablaAlumnos[10];
	int numeroAlumno;

	numeroAlumno = 0;
	do
	{
		imprimirMenu();
		scanf("%i", &opcion);
		switch (opcion)
		{
		case 1: 
			system("CLS");
			cargarDatos(tablaAlumnos, &numeroAlumno);
			break;

		case 2: 
			guardarDatos(tablaAlumnos, numeroAlumno);
			break;

		case 3: 
			system("CLS");
			imprimirPantalla(tablaAlumnos, numeroAlumno);
			break;

		case 4:
			system("CLS");
			insertarDatos(tablaAlumnos);
			break;
		}
	} while (opcion != 6);
    return 0;
}

