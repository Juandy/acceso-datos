import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicheroFileWrite {

	public static void LeerFichero(String nombre, String[] nombres) {
		try {
			// 1. Apertura del fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);

			// 2. Lectura del fichero
			int letra;
			int posicion = 0;
			String cadena = " ";

			letra = fr.read();
			while (letra != -1) {
				// tratamiento del caracter leido
				if (letra == ';') {
					nombres[posicion] = cadena;
					cadena = "";
					posicion++;
				} else
					cadena += ((char) letra);

				// Leer el siguiente caracter
				letra = fr.read();
			}

			// 3. cerrar fichero
			fr.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void EscribirFichero(String nombre, String[] nombres) {
		try {
			// 1. Apertura de fichero
			File fichero = new File("FicheroTexto1.txt");
			FileWriter fw = new FileWriter(fichero);

			// 2. Escribir en el fichero
			for (int i = 0; i < nombres.length; i++) {
				fw.write(nombres[i] + ";");
			}
			// 3. Cerrar fichero
			fw.close();

		} catch (IOException e) {
			// TODO Auto-Generated catch
			System.out.println(e.getMessage());
		}
	}

	public static void InicializarTabla(String[] nombres) {
		for (int i = 0; i < nombres.length; i++) {
			nombres[i] = "";
		}
	}

	public static void EscribirTabla(String[] nombres) {
		System.out.print("Contenido de la tabla: ");
		for (int i = 0; i < nombres.length; i++) {
			System.out.print(nombres[i] + " ");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] listaNombre = { "Pepe", "Laura", "Ana", "Juan" };

		// LLamada a escribir fichero
		EscribirFichero("FicheroTexto1.txt", listaNombre);
		InicializarTabla(listaNombre);
		EscribirTabla(listaNombre);
		// Llamada a leer fichero
		LeerFichero("FicheroTexto1.txt", listaNombre);
		EscribirTabla(listaNombre);

	}

}