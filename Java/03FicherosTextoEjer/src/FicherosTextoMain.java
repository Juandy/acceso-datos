import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FicherosTextoMain {

	public static int menu() {

		Scanner opcion = new Scanner(System.in);

		System.out.println("\nMenu Ficheros");
		System.out.println("********************************");
		System.out.println("1 - Escribir tabla en un fichero");
		System.out.println("2 - Leer fichero");
		System.out.println("3 - Escribir tabla en pantalla");
		System.out.println("4 - Modificar tabla	");
		System.out.println("5 - Actualizar tabla ficheros");
		System.out.println("6 - Salir");
		System.out.println("********************************");
		System.out.print  ("Elija una opcion: ");

		return opcion.nextInt();
	}
	public static void escribirFichero(String nombref,int [] tablaNumeros) {

		String numeros;

		try {
			File fichero = new File (nombref);
			FileWriter fw = new FileWriter(fichero);

			for(int i = 0; i<tablaNumeros.length; i++)
			{
				numeros = Integer.toString (tablaNumeros[i]);
				fw.write(numeros);
				fw.write(' ');
			}
			fw.close();
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public static void escribirFichero2 (String nombref,int [] tablaNumeros) {

		String numeros;

		try {
			File fichero = new File (nombref);
			FileWriter fw = new FileWriter(fichero);

			for(int i = 0; i<tablaNumeros.length; i++)
			{
				numeros = Integer.toString (tablaNumeros[i]);
				fw.write(numeros);
				fw.write("\n");
			}
			fw.close();
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}


	public static void leerFichero (String nombref, int [] tablaNumeros) {
		int i, 
		resultado, 
		numeroE;

		String cadenaNumero;

		try 
		{
			File fichero = new File(nombref);
			FileReader fr = new FileReader(fichero);

			i = 0;
			cadenaNumero = "";
			resultado = fr.read();


			while (resultado != -1) {
				if(resultado == ' ') 
				{
					numeroE = Integer.parseInt(cadenaNumero);
					tablaNumeros[i] = numeroE;
					i++;
					cadenaNumero = "";
				}
				else
					cadenaNumero += (char)resultado;
				resultado = fr.read();			
			}
			System.out.println(cadenaNumero);
			fr.close();
		} catch (FileNotFoundException e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}

	public static void leerFichero2 (String nombref, int [] tablaNumeros) {
		int i, 
		resultado, 
		numeroE;

		String cadenaNumero;

		try 
		{
			File fichero = new File(nombref);
			FileReader fr = new FileReader(fichero);

			i = 0;
			cadenaNumero = "";
			numeroE = 0;
			resultado = fr.read();


			while (resultado != -1) {
				if(resultado == ' ') 
				{
					tablaNumeros[i] = numeroE;
					i++;
					cadenaNumero = "";
				}
				else
					numeroE = (numeroE*10) + (resultado-'0');   //Ej 128  (0x10)+(49-48) 0 + 1 = 1
																//50(2)	  (1x10)+(50-48) 10+2 = 12
																//56(8)   (12x10)+(56-48)120+8=128

					resultado = fr.read();			
			}
			System.out.println(cadenaNumero);
			fr.close();
		} catch (FileNotFoundException e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}
	
	public static void leerFichero3 (String nombref, int [] tablaNumeros) {
		int i, 
		resultado, 
		numeroE;

		String cadenaNumero;

		try 
		{
			File fichero = new File(nombref);
			FileReader fr = new FileReader(fichero);

			i = 0;
			cadenaNumero = "";
			resultado = fr.read();


			while (resultado != -1) {
				if((resultado == '\n') || (resultado == '\n')) 
				{
					numeroE = Integer.parseInt(cadenaNumero);
					tablaNumeros[i] = numeroE;
					i++;
					cadenaNumero = "";
					if(resultado == '\r')
						resultado = fr.read();
				}
				else
					cadenaNumero += (char)resultado;
				resultado = fr.read();
				
			}
			System.out.println(cadenaNumero);
			fr.close();
		} catch (FileNotFoundException e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}

	public static void escribirTablaNumeros(int [] tablaNumeros) {

		for(int i = 0; i<tablaNumeros.length; i++) 
		{
			System.out.print(tablaNumeros[i] + " ");
		}
	}

	public static void modificarTabla(int []tablaNumeros) {

		Scanner numero = new Scanner(System.in);

		int posicionTabla,
		numeroNuevo;

		System.out.print("Posicion en la tabla(min(1) max(10) ): ");
		posicionTabla = numero.nextInt();
		System.out.print("introduce el nuevo nuemro: ");
		numeroNuevo = numero.nextInt();

		tablaNumeros[posicionTabla] = numeroNuevo;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int [] tablaNumeros = {1, 2, 3, 4, 55, 666, 777, 8888, 9, 10000};
		int op;

		do {

			op = menu();
			switch (op) {
			case 1:
				escribirFichero("File\\FicheroTexto.txt", tablaNumeros);
				break;
			case 2:
				leerFichero("File\\FicheroTexto.txt", tablaNumeros);
				leerFichero2("File\\FicheroTexto.txt", tablaNumeros);

				break;
			case 3:
				escribirTablaNumeros(tablaNumeros);
				break;
			case 4:
				modificarTabla(tablaNumeros);

				break;
			case 5:
				System.out.println("La tabla se ha actualizado correctamente");
				escribirTablaNumeros(tablaNumeros);
				break;
			case 6:
				System.exit(0);
				break;

			default:
				System.out.println("Error, opcion no valida (1-6) . ");
				break;
			}



		} while (op != 6);


	}

}
