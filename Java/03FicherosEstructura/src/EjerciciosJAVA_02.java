import java.io.File;

public class EjerciciosJAVA_02 {

	public static void main(String[] args) {
		
		String[] carpetas = {"Nombre","Nombre\\bin",
							 "Nombre\\bin\\bytecode",
							 "Nombre\\bin\\bytecode",
							 "Nombre\\src",
							 "Nombre\\src\\clases",
							 "Nombre\\doc",
							 "Nombre\\doc\\html",
							 "Nombre\\doc\\pdf"};
		
		for(int i = 0; i <carpetas.length; i++) 
		{
			File estructura = new File(carpetas[i]);
			estructura.mkdir();
		}
	}

}
