
public class Metodos {

	/*Metodo*/
	public static int sumar (int n1, int n2) 
	{
		int resultado;
		resultado = n1 + n2;
		return resultado;
	}
	
	public static int sumarNumero (int n1, Numero n)
	{
		int resultado;
		resultado = n1 + n.DevolveValor();
		n1 = 0;
		n.FijarValor(0);
		return resultado;
	}

	public static void main(String[] args) 
	{	
//		int 	numero1 = 23,
//				numero2 = 41,
//				numero3 = 11,
//				res;
//
//		res = sumar(numero1, numero2);		
//		System.out.println("El resultado es " + res);
//
//		res = sumar(numero1, numero3);		
//		System.out.println("El resultado es " + res);
		
		Numero miNumero = new Numero(21),
			   otroNumero = new Numero(33),
			   esteNumero = new Numero (4);
		
		int	   numero1 = 3,
			   resultado;
		
		System.out.println("El primero es "+ miNumero.DevolveValor());
		System.out.println("El segundo es "+ otroNumero.DevolveValor());
		
		resultado = miNumero.Sumar(1);
		System.out.println("La suma es "+ resultado);
		
		//llamada a metodo estatico(static)
		resultado = Numero.Sumar(21, 23);
		System.out.println("La suma es "+ resultado);
		
		//llamada con parametros de clase
		resultado = sumarNumero(numero1, esteNumero);
		System.out.println("numero1 vale " + numero1);
		System.out.println("esteNumero vale " + esteNumero.DevolveValor());
		System.out.println("El resultado es "+ resultado);

		
	}

}
