import java.io.*;

public class Ejer1 {

	public static void LeerFichero(String n, String[] nombres, int[] dni) {
		int resultado, // variable para leer
				i, //
				nEmpleados; // N� empleados de fichero
		String valorCadena; // Convierte datos leidos
		boolean numero, // Indica si lee numero del fichero
				primero; // Indica si lee el primer numero del fichero
		try {
			File fichero = new File(n);
			FileReader fr = new FileReader(fichero);

			numero = true;
			primero = true;
			i = 0;
			valorCadena = "";
			resultado = fr.read();

			while (resultado != -1) {
				if (resultado != '-')
					valorCadena += (char) resultado; // Coge lo que lee y lo mete como cadena
				else {
					if (numero == true) {
						if (primero == true) {
							nEmpleados = Integer.parseInt(valorCadena);
							primero = false;
							// Una vez leido el primer numero lo ponemos en falso
							// para que no lo vuelva a leer ya que solo nos indicara
							// el numero de usuarios en el fichero
						} else {

							dni[i] = Integer.parseInt(valorCadena);
							i++;
						}
						valorCadena = "";
						numero = false;
						// despues de leer un numero lo siguente es un nombre
						// por eso lo ponemos en falso
					} else {
						nombres[i] = valorCadena;
						numero = true;
						// Al igual que antes una vez ledido el nombre pasamos numero a true
						// para que leea el siguiente numero
					}
					valorCadena = "";
				}
				resultado = fr.read();
			}
			fr.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
	
	public static void Escribir(String[]nombres, int[]dni) 
	{
		for (int i = 0; i<nombres.length;i++) 
		{
			System.out.println("El contenido de la tabla es: " + nombres[i] + " - " + dni[i]);
		}
	}
	
	public static void CalcularMayor (String[]nombre, int[]dni) 
	{
		int dniMax, pMax;
		
		dniMax = -10000;
		pMax = 0;
		
		for(int i = 0; i<dni.length; i++) 
		{
			if(dni[i] > dniMax)
			{
				dniMax = dni[i];
				pMax = i;
			}
		}
		System.out.println("El nombre del D.N.I mayor es: " + nombre[pMax]);
	}
	
	public static void CalcularMenor (String[]nombre, int[]dni) 
	{
		int dniMen, pMax;
		
		dniMen = 1000000;
		pMax = 0;
		
		for(int i = 0; i<dni.length; i++) 
		{
			if((dni[i] != 0)&&(dni[i] < dniMen))
			{
				dniMen = dni[i];
				pMax = i;
			}
		}
		System.out.println("El nombre del D.N.I menor es: " + nombre[pMax]);
	}

	public static void main(String[] args) {
		
		String [] nombres = new String [10];
		int [] dni = new int [10];
		
		LeerFichero("Empleados.txt", nombres, dni);
		System.out.println("Datos de fichero");
		Escribir(nombres, dni);
		System.out.println("*************************************");
		
		System.out.println("Dni Mayor ");
		CalcularMayor(nombres, dni);
		System.out.println("*************************************");

		System.out.println("Dni Menor ");
		CalcularMenor(nombres, dni);
		System.out.println("*************************************");

		
	}

}
