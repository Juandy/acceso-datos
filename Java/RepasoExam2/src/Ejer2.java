import java.io.*;
import java.util.Scanner;

public class Ejer2 {

	public static void CrearFicheros(int n1, int n2) {
		try {
			File carpeta; // declaramso la creacin de la carpeta
			File fichero; // fichero

			carpeta = new File(Integer.toString(n2)); // pasamos el numero a un String
			if (carpeta.mkdir() == true) {
				for (int i = n1; i <= n2; i++) {
					fichero = new File(carpeta, Integer.toString(i) + ".num");
					fichero.createNewFile();
				}
			}

		} catch (IOException e) {
			// TODO: handle exception
		}

	}

	public static void main(String[] args) {
		File carpeta,fichero;
		int n1,n2;
		
		Scanner leer = new Scanner(System.in);
		
		System.out.print("Introduce el primer numero: ");
		n1 = leer.nextInt();
		System.out.print("Introduce el segundo numero: ");
		n2 = leer.nextInt();
		
		CrearFicheros(n1, n2);
		

	}

}
