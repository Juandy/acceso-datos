
public class EscrituraLectura {
	//Escribir la tabla ordenada 
	//-por edad
	//Escribir la tabla ordenada
	//-por nombre

	public static void MostrarPantalla(Persona[] Tabla) {
		for (int i = 0; i < Tabla.length; i++) {
			System.out.println("Nombre " + Tabla[i].getNombre() + " Edad " + Tabla[i].getEdad());
		}
	}

	public static void VaciarTabla(Persona[] tabla) {
		for (int i = 0; i < tabla.length; i++) {
			// Cambiando los valores del objeto
			tabla[i].setNombre("");
			tabla[i].setEdad(0);
			// Creando objetos nuevos
			 //tabla[i] = new Persona("NINGUNO",100); creas un objeto nuevo con esos
			// parametros
			tabla[i] = new Persona(); // Constructor por defecto
		}

	}

	public static void main(String[] args) {

		String[] miTabla = { "juan", "Ana Maria", "pedro", "Laura", "Pepe", "Arturo Fernandez" };
		String[] otraTabla = new String[10];

		FicheroEnBuffer miFichero = new FicheroEnBuffer("nombre.txt");
		miFichero.EscribirTabla(miTabla);
		miFichero.LeerTabla(otraTabla);
		for (int i = 0; i < otraTabla.length; i++)
			System.out.println("Pos" + i + " " + otraTabla[i]);

		// Tabla Personas
		Persona[] misPersonas = new Persona[4];
		misPersonas[0] = new Persona("Juan", 210);
		misPersonas[1] = new Persona("Ana Isabel", 20);
		misPersonas[2] = new Persona("Lorenzo", 20);
		misPersonas[3] = new Persona("Arturo Fernandez", 19);

		MostrarPantalla(misPersonas);

		FicheroAlumno miFicheroA = new FicheroAlumno("FAlumnos.txt");
		miFicheroA.EscribirTabla(misPersonas);

		System.out.println("\n");
		System.out.println("Vavia tabla y lo muestra");
		VaciarTabla(misPersonas);
		MostrarPantalla(misPersonas);

		System.out.println("\n");
		System.out.println("Lee el archivo de nuevo y lo escribe");
		miFicheroA.LeerTabla(misPersonas);
		MostrarPantalla(misPersonas);
		
		System.out.println("Ordenar valores");
		FicheroAlumno miFicheroAl = new FicheroAlumno("Alumno.txt");
		miFicheroAl.LeerTabla(misPersonas);
		FicheroEnBuffer miFicheroOrdenado = new FicheroEnBuffer("FAlumnos.txt");
		miFicheroOrdenado.EscribirTabla(miTabla);
	}
}
