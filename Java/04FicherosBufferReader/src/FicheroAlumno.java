import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicheroAlumno {
	
	private String nombreFichero;
	
	public FicheroAlumno(String n) 
	{
		nombreFichero = n;
	}
	
	public  void EscribirTabla(Persona[]Tabla)
	{
		try {
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i = 0; i<Tabla.length;i++) 
			{
				bw.write(Tabla[i].getNombre());
				bw.newLine();
				bw.write(Integer.toString(Tabla[i].getEdad())); // Pasamos la edad a una cadena
				bw.newLine();
			}
			bw.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void LeerTabla(Persona[]Tabla) 
	{	
		try {
			File fichero = new File(nombreFichero);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			
			String linea;
			int i;
			i=0;
			linea = br.readLine();//lee la primera linea
//			String nombre;
//			int edad;      Variables para el MetodoV2
			
			while (linea != null) 
			{
				//MetodoV1 son setter
				Tabla[i].setNombre(linea);					// lo lee y lo guarda en a tabla y pasa a la siguiente linea
				linea = br.readLine();						//Lee la siguiente linea
				Tabla[i].setEdad(Integer.parseInt(linea)); 	//Lee la edad y lo pasa a una cadena
				i++;
				linea = br.readLine();
				
//				//MetodoV2 con constrctores
//				nombre = linea;
//				edad= Integer.parseInt(linea);
//				Tabla[i] = new Persona(nombre,edad);
//				i++;
//				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	

}
