import java.io.*;
public class FicheroEnBuffer 
{
	private String nombreFichero;

	public FicheroEnBuffer(String nombre)
	{
		nombreFichero=nombre;
	}

	public void EscribirTabla(String [] tabla) 
	{
		try {
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);

			for(int i = 0; i<tabla.length;i++) 
			{
				bw.write(tabla[i]); //escribe un nombre por l�nea
				bw.newLine();		//hace un salto de linea
			}
			bw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

	}
	public void LeerTabla(String [] tabla) 
	{
		try {
			File fichero = new File(nombreFichero);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			
			String linea;
			int i;
			i=0;
			linea = br.readLine();
			
			while (linea != null) 
			{
				tabla[i] = linea;
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

	}
	public void EscribirTablaOrdenada(Persona [] tabla) 
	{
		try {
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			Persona personaMinAnt = new Persona("", -100);
			Persona personaMinAct = new Persona("", 10000);

			for(int i = 0; i<tabla.length;i++) 
			{
			
				for(int j=0;i<tabla.length;j++) 
				{
					if( (tabla[j].getEdad() > personaMinAnt.getEdad() )&&
					    (tabla[j].getEdad() < personaMinAct.getEdad() ) ) 
					{
						personaMinAct = tabla[j];
					} 
				}
				bw.write(personaMinAct.getNombre());
				bw.newLine();
				bw.write(Integer.toString(personaMinAct.getEdad()));
				bw.newLine();
				personaMinAnt = personaMinAct;
				personaMinAct.setEdad(1000000);
			}
			bw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

	}
	

}
