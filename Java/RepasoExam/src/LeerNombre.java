import java.io.*;
import java.util.Scanner;

public class LeerNombre {

	public static void EncontrarFichero(String n, Nombre[] nombre) {
		try {
			File fichero = new File(n);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);

			String linea, name;
			int i = 0;

			linea = br.readLine();
			while (linea != null) {
				nombre[i] = new Nombre(linea);
				i++;
				linea = br.readLine();

			}
			br.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void main(String[] args) {
		Nombre[] nombre = new Nombre[4];
		String nombreBuscado;
		boolean encontrado = false;
		int contador = 0;
		Scanner fn = new Scanner(System.in);
		EncontrarFichero("Nombres.txt", nombre);

		System.out.print("Que nombre desea buscar: ");
		nombreBuscado = fn.next();

		if (encontrado == false) {
			for (int i = 0; i < nombre.length; i++) {
				if (nombreBuscado.equals(nombre[i].getNombre() ) ) {
					System.out.println("El nombre aparece en la posicion " + i + " del fichero");
				}else{
					contador++ ;
				}
			}
			if(contador == nombre.length)
				System.out.println("El nombre no esta:");
		}
	}
}
