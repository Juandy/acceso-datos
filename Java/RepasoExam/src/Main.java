import java.io.*;

public class Main {

	public static void leerCategoria(String n, Categoria[] tCat) {
		try {
			File fichero = new File(n);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);

			String linea, idCat, nombre;
			int i = 0;

			linea = br.readLine();

			while (linea != null) {
				idCat = linea.toString();
				nombre = br.readLine();
				tCat[i] = new Categoria(idCat, nombre);
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void leerProducto(String n, Productos[] tProd) {
		try {
			File fichero = new File(n);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);

			String linea, idProd, nombre, idCat;
			int i = 0;
			linea = br.readLine();

			while (linea != null) {
				idProd = linea.toString();
				nombre = br.readLine();
				idCat = br.readLine().toString();
				tProd[i] = new Productos(idProd, nombre, idCat);
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void main(String[] args) {
		Categoria[] categoria = new Categoria[5];
		Productos[] productos = new Productos[7];

		leerCategoria("FicheroCategoria.txt", categoria);
		leerProducto("FicheroProducto.txt", productos);

		String id;

		int np = 0;
		String idCat, idPro;
		for (int i = 0; i < categoria.length; i++) {
			np = 0;
			idCat = categoria[i].getIdCategoria();

			for (int j = 0; j < productos.length; j++) {
				idPro = productos[j].getIdCategoria();
				if (idCat.equals(idPro))
					np++;
			}
			System.out.println("Categorias " + categoria[i].getNombre() + " " + np);
		}
	}

}
