
public class Categoria {
	private String idCategoria, nombre;

	public Categoria(String id, String n) {
		idCategoria = id;
		nombre = n;
	}

	public String getIdCategoria () 
	{
		return idCategoria;
	}
	
	public String getNombre() 
	{
		return nombre;
	}

}
