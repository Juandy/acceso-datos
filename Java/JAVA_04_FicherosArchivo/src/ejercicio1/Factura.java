package ejercicio1;

public class Factura {
	
	private int idFactura;
	private int nArticulos;
	
	public Factura () {
		
	}
	
	public Factura (int id, int nA) {
		
		idFactura = id;
		nArticulos = nA;
		
	}

	public int getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}

	public int getnArticulos() {
		return nArticulos;
	}

	public void setnArticulos(int nArticulos) {
		this.nArticulos = nArticulos;
	}

}
